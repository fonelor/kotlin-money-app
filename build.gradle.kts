import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.net.URI

subprojects {

    ext {
        set("hsqldbVersion", "2.5.0")
        set("junitVersion", "5.4.2")
        set("postgresVersion", "42.2.5")
        set("liquibaseVersion", "3.6.3")
        set("jooqVersion", "3.11.11")
        set("koinVersion", "2.0.1")
        set("ktorVersion", "1.2.1")
        set("jacksonVersion", "2.9.9")
        set("hikariVersion", "3.3.1")
        set("logbackVersion", "1.2.3")
        set("kotlintestVersion", "3.3.2")
    }

    repositories {
        mavenCentral()
        jcenter()
        maven { url = URI("https://dl.bintray.com/kotlin/kotlinx") }
        maven { url = URI("https://dl.bintray.com/kotlin/ktor") }
    }

}


plugins {
    kotlin("jvm") version "1.3.31"
}

group = "ru.rolsoft.moneyapp"
version = "1.1"

repositories {
    mavenCentral()
    jcenter()
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "11"
    }
}