import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.net.URI

val distOutDir = "${project.buildDir}/distributions/out"

plugins {
    kotlin("jvm")
    application
    id("com.heroku.sdk.heroku-gradle") version "1.0.4"
    id("com.github.johnrengelman.shadow") version "5.1.0"
}

val stressTest by sourceSets.creating {
    withConvention(KotlinSourceSet::class) {
        kotlin.srcDir("src/stress-test/kotlin")
        compileClasspath += sourceSets["main"].output.classesDirs
        runtimeClasspath += sourceSets["main"].output.classesDirs
    }
}

val intTest by sourceSets.creating {
    withConvention(KotlinSourceSet::class) {
        kotlin.srcDir("src/int-test/kotlin")
        compileClasspath += sourceSets["main"].output.classesDirs
        runtimeClasspath += sourceSets["main"].output.classesDirs
    }
}

val stressTestImplementation by configurations.getting {
    extendsFrom(configurations["implementation"])
}

val intTestImplementation by configurations.getting {
    extendsFrom(configurations["implementation"])
}


application {
    mainClassName = "ru.rolsoft.moneyapp.service.accounts.MoneyAppKt"
}

heroku {
    appName = "service-accounts"
    includes = listOf(distOutDir)
    includeBuildDir = false
    processTypes["web"] = "service-accounts/bin/service-accounts"
    includeRootDir = File(distOutDir)
    jdkVersion = "11"
}

repositories {
    maven { url = URI("https://dl.bintray.com/devexperts/Maven") }
}

dependencies {
    implementation(project(":database"))

    implementation(kotlin("stdlib-jdk8"))

    implementation("org.koin:koin-ktor:${project.extra["koinVersion"]}")
    implementation("io.ktor:ktor-server-netty:${project.extra["ktorVersion"]}")
    implementation("io.ktor:ktor-jackson:${project.extra["ktorVersion"]}")

    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jdk8:${project.extra["jacksonVersion"]}")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:${project.extra["jacksonVersion"]}")

    implementation("ch.qos.logback:logback-classic:${project.extra["logbackVersion"]}")
    implementation("org.hsqldb:hsqldb:${project.extra["hsqldbVersion"]}")

    testImplementation("io.kotlintest:kotlintest-runner-junit5:${project.extra["kotlintestVersion"]}")
    testImplementation(project(":database", "tests"))

    stressTestImplementation("io.kotlintest:kotlintest-runner-junit5:${project.extra["kotlintestVersion"]}")
    stressTestImplementation(project(":database", "tests"))

    intTestImplementation("io.kotlintest:kotlintest-runner-junit5:${project.extra["kotlintestVersion"]}")
    intTestImplementation("io.ktor:ktor-server-test-host:${project.extra["ktorVersion"]}")
    intTestImplementation(project(":database", "tests"))
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "11"
    }

    withType<Test> {
        useJUnitPlatform()
    }

    val unzipDist by registering(Copy::class) {
        dependsOn(distZip)

        val zipFiles = layout.files(distZip.get())

        from(zipFiles.map { zipTree(it) })
        into(File(distOutDir))
    }

    val stressTest by registering(Test::class) {
        group = "verification"
        testClassesDirs = stressTest.output.classesDirs
        classpath = stressTest.runtimeClasspath
    }

    val intTest by registering(Test::class) {
        group = "verification"
        testClassesDirs = intTest.output.classesDirs
        classpath = intTest.runtimeClasspath
    }

    val herokuDeploy = getByName("deployHeroku")
    herokuDeploy.dependsOn(unzipDist)
}