package ru.rolsoft.moneyapp.service.accounts

import io.kotlintest.matchers.collections.shouldHaveSize
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrowUnit
import ru.rolsoft.moneyapp.database.TransactionType
import ru.rolsoft.moneyapp.database.db.tables.daos.DbListener.accountsDao
import ru.rolsoft.moneyapp.database.db.tables.daos.DbListener.createUserWith2Accounts
import ru.rolsoft.moneyapp.database.db.tables.daos.DbListener.transactionsDao
import ru.rolsoft.moneyapp.database.db.tables.daos.DbSpec
import ru.rolsoft.moneyapp.service.accounts.account.NoSuchAccountException
import ru.rolsoft.moneyapp.service.accounts.transaction.NotEnoughMoneyException
import ru.rolsoft.moneyapp.service.accounts.transaction.TransactionException
import ru.rolsoft.moneyapp.service.accounts.transaction.TransactionService
import java.math.BigDecimal

class TransactionServiceTest : DbSpec({

    should("transfer from one account to another") {
        val accounts = createUserWith2Accounts(BigDecimal.TEN, BigDecimal.ZERO)

        val transactionService = TransactionService(accountsDao, transactionsDao)
        val transfer =
            transactionService.transfer(accounts[0], accounts[1], BigDecimal.ONE)


        transfer.apply {
            source.shouldBe(accounts[0])
            sink.shouldBe(accounts[1])
            amount.shouldBe(BigDecimal.ONE.setScale(3))
            type.shouldBe(TransactionType.TRANSFER.name)
        }

        accountsDao.fetchOneById(accounts[0]).apply {
            balance.shouldBe(BigDecimal(9).setScale(3))
            version.shouldBe(2)
        }

        accountsDao.fetchOneById(accounts[1]).apply {
            balance.shouldBe(BigDecimal.ONE.setScale(3))
            version.shouldBe(2)
        }
    }
    should("throw not enough money") {
        val accounts = createUserWith2Accounts(BigDecimal.TEN, BigDecimal.ZERO)

        val transactionService = TransactionService(accountsDao, transactionsDao)
        shouldThrowUnit<NotEnoughMoneyException> {
            transactionService.transfer(accounts[1], accounts[0], BigDecimal.ONE)
        }
    }
    should("throw amount of 0 is not allowed") {
        val accounts = createUserWith2Accounts(BigDecimal.TEN, BigDecimal.ZERO)

        val transactionService = TransactionService(accountsDao, transactionsDao)
        shouldThrowUnit<TransactionException> {
            transactionService.transfer(accounts[1], accounts[0], BigDecimal.ZERO)
        }
    }
    should("throw no such account") {
        val transactionService = TransactionService(accountsDao, transactionsDao)
        shouldThrowUnit<NoSuchAccountException> {
            transactionService.transfer(1L, 2L, BigDecimal.ONE)
        }
    }
    should("deposit") {
        val accounts = createUserWith2Accounts(BigDecimal.TEN, BigDecimal.ZERO)

        val transactionService = TransactionService(accountsDao, transactionsDao)
        transactionService.deposit(accounts[0], BigDecimal.ONE).second.apply {
            version.shouldBe(2L)
            balance.shouldBe(BigDecimal(11).setScale(3))
        }

        transactionService.getTransactions(accounts[0], 100, null).apply {
            deposits.shouldHaveSize(1)
            deposits[0].apply {
                amount.shouldBe(BigDecimal.ONE.setScale(3))
                sink.shouldBe(accounts[0])
                source.shouldBe(accounts[0])
                type.shouldBe(TransactionType.DEPOSIT.name)
            }
            withdraws.shouldHaveSize(0)
            transfers.shouldHaveSize(0)
        }

    }
    should("withdraw") {
        val accounts = createUserWith2Accounts(BigDecimal.TEN, BigDecimal.ZERO)

        val transactionService = TransactionService(accountsDao, transactionsDao)
        transactionService.withdraw(accounts[0], BigDecimal.TEN).second.apply {
            version.shouldBe(2L)
            balance.shouldBe(BigDecimal.ZERO.setScale(3))
        }

        transactionService.getTransactions(accounts[0], 100, null).apply {
            withdraws.shouldHaveSize(1)
            withdraws[0].apply {
                amount.shouldBe(BigDecimal.TEN.setScale(3))
                sink.shouldBe(accounts[0])
                source.shouldBe(accounts[0])
                type.shouldBe(TransactionType.WITHDRAW.name)
            }
            deposits.shouldHaveSize(0)
            transfers.shouldHaveSize(0)
        }
    }
    should("return withdraws, deposits and transfers") {
        val accounts = createUserWith2Accounts(BigDecimal.TEN, BigDecimal.ZERO)

        val transactionService = TransactionService(accountsDao, transactionsDao)
        transactionService.deposit(accounts[0], BigDecimal.ONE)
        transactionService.withdraw(accounts[0], BigDecimal.TEN)
        transactionService.transfer(accounts[0], accounts[1], BigDecimal.ONE)

        transactionService.getTransactions(accounts[0], 100, null).apply {
            withdraws.shouldHaveSize(1)
            deposits.shouldHaveSize(1)
            transfers.shouldHaveSize(1)
        }

        transactionService.getTransactions(accounts[1], 100, null).apply {
            withdraws.shouldHaveSize(0)
            deposits.shouldHaveSize(0)
            transfers.shouldHaveSize(1)
        }
    }
})