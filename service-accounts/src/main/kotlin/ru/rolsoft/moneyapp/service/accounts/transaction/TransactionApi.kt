package ru.rolsoft.moneyapp.service.accounts.transaction

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.header
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import org.koin.ktor.ext.inject
import ru.rolsoft.moneyapp.service.accounts.badRequest
import java.math.BigDecimal

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
const val TRANSACTION_ID_HEADER = "X-Transaction-Id"

fun Route.transactionApi() {

    val transactionService by inject<TransactionService>()

    route("transactions") {
        get {
            val accountId = call.request.queryParameters["accountId"]
            val pageSize = call.request.queryParameters["pageSize"]
            val lastId = call.request.queryParameters["lastId"]
            try {
                if (accountId.isNullOrEmpty()) {
                    badRequest("Account id should be submitted")
                } else {
                    val transactions = transactionService.getTransactions(accountId.toLong(),
                            pageSize?.toInt(),
                            lastId?.toLong())
                    call.respond(transactions)
                }
            } catch (ex: NumberFormatException) {
                badRequest("Account id should be long value")
            }
        }

        post {
            val transferRequest = call.receive<TransferRequest>()
            val transaction = transactionService.transfer(transferRequest.fromAcc, transferRequest.toAcc, transferRequest.amount)

            call.response.header(TRANSACTION_ID_HEADER, transaction.id)
            call.respond(HttpStatusCode.Created, transaction)
        }

        get("{id}") {
            try {
                val transactionId = call.parameters["id"]?.toLong()
                if (transactionId == null) {
                    badRequest("Transaction id should not be null")
                } else {
                    call.respond(transactionService.getTransaction(transactionId))
                }
            } catch (ex: NumberFormatException) {
                badRequest("Transaction id should be long value")
            }
        }

        post("withdraw") {
            val withdrawRequest = call.receive<WithdrawRequest>()
            val txIdToAccount = transactionService.withdraw(withdrawRequest.accountId, withdrawRequest.amount)

            call.response.header(TRANSACTION_ID_HEADER, txIdToAccount.first)
            call.respond(HttpStatusCode.Created, txIdToAccount.second)
        }

        post("deposit") {
            val depositRequest = call.receive<DepositRequest>()
            val txIdToAccount = transactionService.deposit(depositRequest.accountId, depositRequest.amount)

            call.response.header(TRANSACTION_ID_HEADER, txIdToAccount.first)
            call.respond(HttpStatusCode.Created, txIdToAccount.second)
        }
    }
}

data class TransferRequest(val fromAcc: Long, val toAcc: Long, val amount: BigDecimal)
data class WithdrawRequest(val accountId: Long, val amount: BigDecimal)
data class DepositRequest(val accountId: Long, val amount: BigDecimal)