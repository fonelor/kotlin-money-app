package ru.rolsoft.moneyapp.service.accounts.account

import ru.rolsoft.moneyapp.database.daos.AccountsDaoExt
import ru.rolsoft.moneyapp.database.daos.UsersDaoExt
import ru.rolsoft.moneyapp.database.daos.insertReturningId
import ru.rolsoft.moneyapp.database.db.tables.pojos.AccountsEntity
import ru.rolsoft.moneyapp.database.newAccount
import ru.rolsoft.moneyapp.service.accounts.MoneyAppException
import ru.rolsoft.moneyapp.service.accounts.user.NoSuchUserException

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
class AccountsService(
        private val usersDao: UsersDaoExt,
        private val accountsDaoExt: AccountsDaoExt) {

    fun createAccount(userId: Long): AccountsEntity {
        checkUserExists(userId)
        val accId = accountsDaoExt.insertReturningId(newAccount(userId))
        return accountsDaoExt.fetchOneById(accId)
    }

    fun getAccounts(userId: Long): List<AccountsEntity> {
        checkUserExists(userId)
        return accountsDaoExt.fetchByUserId(userId)
    }

    fun getAccount(accountId: Long): AccountsEntity {
        return accountsDaoExt.fetchOneById(accountId) ?: throw NoSuchAccountException(accountId)
    }

    private fun checkUserExists(userId: Long) {
        if (!usersDao.existsById(userId)) throw NoSuchUserException(userId)
    }

}

class NoSuchAccountException(accountId: Long) : MoneyAppException("No account with id $accountId")
