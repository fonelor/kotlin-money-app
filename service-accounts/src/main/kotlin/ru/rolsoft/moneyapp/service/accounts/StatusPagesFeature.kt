package ru.rolsoft.moneyapp.service.accounts

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.exc.InvalidFormatException
import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.request.path
import io.ktor.response.respond
import io.ktor.util.pipeline.PipelineContext
import ru.rolsoft.moneyapp.service.accounts.account.NoSuchAccountException
import ru.rolsoft.moneyapp.service.accounts.transaction.NoSuchTransactionException
import ru.rolsoft.moneyapp.service.accounts.user.NoSuchUserException

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
fun Application.installStatusPages() {
    install(StatusPages) {
        exception<NoSuchUserException> { cause ->
            notFound(cause.localizedMessage)
        }
        exception<NoSuchAccountException> { cause ->
            notFound(cause.localizedMessage)
        }
        exception<NoSuchTransactionException> { cause ->
            notFound(cause.localizedMessage)
        }

        exception<JsonParseException> { cause ->
            badRequest("invalid json: " + cause.localizedMessage)
        }
        exception<InvalidFormatException> { cause ->
            badRequest("invalid json: " + cause.localizedMessage)
        }

        exception<MoneyAppException> { cause ->
            badRequest(cause.localizedMessage)
        }
        exception<Exception> { cause ->
            call.respond(HttpStatusCode.InternalServerError,
                    StatusPageError(HttpStatusCode.InternalServerError, cause.localizedMessage))
        }

        status(HttpStatusCode.NotFound) {
            call.respond(HttpStatusCode.NotFound, StatusPageError(HttpStatusCode.NotFound, "Incorrect path: ${call.request.path()}"))
        }
    }
}

suspend fun PipelineContext<Unit, ApplicationCall>.notFound(message: String) {
    call.respond(HttpStatusCode.NotFound, StatusPageError(HttpStatusCode.NotFound, message))
}

suspend fun PipelineContext<Unit, ApplicationCall>.badRequest(message: String) {
    call.respond(HttpStatusCode.BadRequest, StatusPageError(HttpStatusCode.BadRequest, message))
}

data class StatusPageError(val code: Int,
                           val description: String,
                           val message: String) {
    constructor(code: HttpStatusCode, message: String) : this(code.value, code.description, message)
}