package ru.rolsoft.moneyapp.service.accounts.user

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.*
import org.koin.ktor.ext.inject
import ru.rolsoft.moneyapp.service.accounts.account.AccountsService
import ru.rolsoft.moneyapp.service.accounts.badRequest

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
fun Route.userApi() {
    val userService by inject<UserService>()
    val accountsService by inject<AccountsService>()

    route("users") {
        get("{id}") {
            try {
                val userId = call.parameters["id"]
                if (userId == null) {
                    badRequest("User id should be Long")
                } else {
                    val user = userService.getUser(userId.toLong())
                    call.respond(user)
                }
            } catch (nfe: NumberFormatException) {
                badRequest("User id should be long value")
            }
        }

        get("{id}/accounts") {
            try {
                val userId = call.parameters["id"]
                if (userId == null) {
                    badRequest("User id should not be null/empty")
                } else {
                    val accounts = accountsService.getAccounts(userId.toLong())
                    call.respond(accounts)
                }
            } catch (nfe: NumberFormatException) {
                badRequest("User id should be long value")
            }
        }

        post {
            val createUser = call.receive<UserRequest>()

            when {
                createUser.firstName.isNullOrEmpty() -> badRequest("firstName should not be null/empty")
                createUser.lastName.isNullOrEmpty() -> badRequest("lastName should not be null/empty")
                else -> {
                    val createdUser = userService.createUser(createUser.firstName, createUser.lastName)
                    call.respond(HttpStatusCode.Created, createdUser)
                }
            }
        }

        patch("{id}") {
            try {
                val userId = call.parameters["id"]
                if (userId == null) {
                    badRequest("User id should not be null/empty")
                } else {
                    val userRequest = call.receive<UserRequest>()
                    val updatedUser = userService.updateUser(userId.toLong(), userRequest.firstName, userRequest.lastName)
                    call.respond(updatedUser)
                }
            } catch (nfe: NumberFormatException) {
                badRequest("User id should be long value")
            }
        }
    }

}

data class UserRequest(val firstName: String?, val lastName: String?)