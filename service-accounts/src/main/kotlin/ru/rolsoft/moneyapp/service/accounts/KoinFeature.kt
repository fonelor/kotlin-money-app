package ru.rolsoft.moneyapp.service.accounts

import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.config.ApplicationConfig
import org.koin.core.module.Module
import org.koin.dsl.module
import org.koin.ktor.ext.Koin
import org.slf4j.LoggerFactory
import ru.rolsoft.moneyapp.database.*
import ru.rolsoft.moneyapp.database.daos.AccountsDaoExt
import ru.rolsoft.moneyapp.database.daos.TransactionsDaoExt
import ru.rolsoft.moneyapp.database.daos.UsersDaoExt
import ru.rolsoft.moneyapp.service.accounts.account.AccountsService
import ru.rolsoft.moneyapp.service.accounts.transaction.TransactionService
import ru.rolsoft.moneyapp.service.accounts.user.UserService

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
fun Application.installKoin() {
    install(Koin) {
        modules(listOf(
                dbModule(environment.config.config("db")),
                serviceModule))
        createEagerInstances()
    }
}

fun dbModule(config: ApplicationConfig): Module = module(createdAtStart = true) {
    single(createdAtStart = true) {
        val logger = LoggerFactory.getLogger("config")
        val dbConfig = DbConfig(
                "${config.property("jdbcUrl").getString()}?currentSchema=${DBConstants.SCHEMA_NAME}",
                config.property("username").getString(),
                config.property("password").getString(),
                10)
        logger.info(dbConfig.toString())
        return@single dbConfig
    }
    single(createdAtStart = true) { dslContext(get()) }
    single(createdAtStart = true) { configuration(dataSource(get())) }

    single { AccountsDaoExt(get()) }
    single { TransactionsDaoExt(get()) }
    single { UsersDaoExt(get()) }
}

val serviceModule = module(createdAtStart = true) {
    single(createdAtStart = true) { TransactionService(get(), get()) }
    single(createdAtStart = true) { AccountsService(get(), get()) }
    single(createdAtStart = true) { UserService(get()) }
}
