package ru.rolsoft.moneyapp.service.accounts

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.jackson.jackson
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
fun Application.installJackson() {
    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
            disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            registerModule(Jdk8Module())
            registerModule(JavaTimeModule())
            registerModule(KotlinModule())

            // add custom serializer for big decimal
            val simpleModule = SimpleModule()
            simpleModule.addSerializer(BigDecimal::class.java, BigDecimalSerializer())
            registerModule(simpleModule)
        }
    }
}

class BigDecimalSerializer : JsonSerializer<BigDecimal>() {
    override fun serialize(value: BigDecimal, jsonGenerator: JsonGenerator, serializerProvider: SerializerProvider) {
        jsonGenerator.writeString(value.setScale(2, RoundingMode.HALF_UP).toString())
    }
}