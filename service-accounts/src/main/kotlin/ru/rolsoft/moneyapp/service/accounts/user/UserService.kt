package ru.rolsoft.moneyapp.service.accounts.user

import ru.rolsoft.moneyapp.database.NoSuchEntityException
import ru.rolsoft.moneyapp.database.daos.UsersDaoExt
import ru.rolsoft.moneyapp.database.daos.insertReturningId
import ru.rolsoft.moneyapp.database.db.tables.pojos.UsersEntity
import ru.rolsoft.moneyapp.database.newUser
import ru.rolsoft.moneyapp.service.accounts.MoneyAppException

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
class UserService(private val usersDao: UsersDaoExt) {

    fun createUser(firstName: String, lastName: String): UsersEntity {
        val userId = usersDao.insertReturningId(newUser(firstName, lastName))
        return usersDao.fetchOneById(userId)
    }

    fun updateUser(id: Long, firstName: String?, lastName: String?): UsersEntity {
        try {
            return usersDao.updateUser(id, firstName, lastName)
        } catch (noUser: NoSuchEntityException) {
            throw NoSuchUserException(id)
        }
    }

    fun getUser(id: Long): UsersEntity {
        return usersDao.fetchOneById(id) ?: throw NoSuchUserException(id)
    }

}

class NoSuchUserException(id: Long) : UserServiceException("User with id $id is not found")

open class UserServiceException(msg: String) : MoneyAppException(msg)
