package ru.rolsoft.moneyapp.service.accounts.transaction

import org.jooq.ContextTransactionalCallable
import org.slf4j.LoggerFactory
import ru.rolsoft.moneyapp.database.TransactionType
import ru.rolsoft.moneyapp.database.daos.AccountsDaoExt
import ru.rolsoft.moneyapp.database.daos.TransactionsDaoExt
import ru.rolsoft.moneyapp.database.daos.inTransactionWithRetry
import ru.rolsoft.moneyapp.database.daos.insertReturningId
import ru.rolsoft.moneyapp.database.db.tables.pojos.AccountsEntity
import ru.rolsoft.moneyapp.database.db.tables.pojos.TransactionsEntity
import ru.rolsoft.moneyapp.database.depositTransaction
import ru.rolsoft.moneyapp.database.transferTransaction
import ru.rolsoft.moneyapp.database.withdrawTransaction
import ru.rolsoft.moneyapp.service.accounts.MoneyAppException
import ru.rolsoft.moneyapp.service.accounts.account.NoSuchAccountException
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
class TransactionService(
        private val accountsDao: AccountsDaoExt,
        private val transactionsDao: TransactionsDaoExt) {
    private val log = LoggerFactory.getLogger(TransactionService::class.java)

    fun getTransaction(transactionId: Long): TransactionsEntity {
        return transactionsDao.fetchOneById(transactionId) ?: throw NoSuchTransactionException(transactionId)
    }

    fun getTransactions(accId: Long, pageSize: Int?, lastId: Long?): TransactionsHistory {
        getAccount(accId)

        log.debug("Get all transactions by account id: $accId, page size: $pageSize, last id : $lastId")
        val transactions = transactionsDao.fetchByAccountId(accId, pageSize ?: 100, lastId)

        val grouped = transactions.groupBy(TransactionsEntity::getType)

        log.debug("Found transactions $grouped")

        return TransactionsHistory(grouped[TransactionType.TRANSFER.name].orEmpty(),
                grouped[TransactionType.DEPOSIT.name].orEmpty(),
                grouped[TransactionType.WITHDRAW.name].orEmpty())
    }

    fun deposit(accId: Long, amount: BigDecimal): Pair<Long, AccountsEntity> {
        checkAmount(amount)
        log.debug("Deposit transaction account: $accId, amount: $amount")
        // check that account exists
        getAccount(accId)

        return transactionsDao.inTransactionWithRetry(ContextTransactionalCallable {
            // create transaction
            val txId = transactionsDao.insertReturningId(depositTransaction(accId, amount))
            // update account
            val accountUpdated = accountsDao.updateWithVersionIncrement(accId, amount)

            log.debug("Successfully updated account $accountUpdated")

            return@ContextTransactionalCallable txId to accountUpdated
        })
    }

    fun withdraw(accId: Long, amount: BigDecimal): Pair<Long, AccountsEntity> {
        checkAmount(amount)
        log.debug("Withdraw transaction account: $accId, amount: $amount")
        return transactionsDao.inTransactionWithRetry(ContextTransactionalCallable {
            val account = checkAccBalance(accId, amount)
            // create transaction
            val txId = transactionsDao.insertReturningId(withdrawTransaction(accId, amount))
            // update account
            val accountUpdated = accountsDao.updateWithVersionIncrement(accId, amount.negate(), account.version)

            log.debug("Successfully updated account $accountUpdated")

            return@ContextTransactionalCallable txId to accountUpdated
        })
    }

    fun transfer(fromAccId: Long, toAccId: Long, amount: BigDecimal): TransactionsEntity {
        checkAmount(amount)
        if (fromAccId == toAccId) throw NotATransferTransactionException(fromAccId)
        log.debug("Transfer transaction from account: $fromAccId, to account: $toAccId, amount: $amount")
        return transactionsDao.inTransactionWithRetry(ContextTransactionalCallable {
            val fromAccount = checkAccBalance(fromAccId, amount)
            val toAcc = accountsDao.fetchOneById(toAccId) ?: throw NoSuchAccountException(toAccId)
            // create transaction
            val txId = transactionsDao.insertReturningId(transferTransaction(fromAccId, toAccId, amount))
            // update source account
            val updatedFromAccount = accountsDao.updateWithVersionIncrement(fromAccount.id, amount.negate(), fromAccount.version)
            // update sink account
            val updatedToAccount = accountsDao.updateWithVersionIncrement(toAcc.id, amount, toAcc.version)

            val transaction = transactionsDao.fetchOneById(txId)

            log.debug("Successfully updated account $updatedFromAccount")
            log.debug("Successfully updated account $updatedToAccount")
            log.debug("Successfully created transaction $transaction")

            return@ContextTransactionalCallable transaction!!
        })
    }

    private fun checkAccBalance(accId: Long, amount: BigDecimal): AccountsEntity {
        val account = getAccount(accId)
        if (account.balance.setScale(3, RoundingMode.HALF_UP) < amount.setScale(3, RoundingMode.HALF_UP))
            throw NotEnoughMoneyException(accId, amount)
        return account
    }

    private fun checkAmount(amount: BigDecimal) {
        if (amount.setScale(3) <= BigDecimal.ZERO.setScale(3)) throw TransactionException("transaction amount should be more than 0")
    }

    private fun getAccount(accId: Long): AccountsEntity = accountsDao.fetchOneById(accId) ?: throw NoSuchAccountException(accId)

}

data class TransactionsHistory(val transfers: List<TransactionsEntity>,
                               val deposits: List<TransactionsEntity>,
                               val withdraws: List<TransactionsEntity>)

class NoSuchTransactionException(transactionId: Long) :
        TransactionException("Transaction $transactionId not found")

class NotATransferTransactionException(fromAccId: Long) :
        TransactionException("fromAcc is the same as toAcc: $fromAccId. Use withdraw or deposit")

class NotEnoughMoneyException(accId: Long, amount: BigDecimal) : TransactionException("$accId has not enough money to transfer $amount")

open class TransactionException(msg: String) : MoneyAppException(msg)
