package ru.rolsoft.moneyapp.service.accounts

import com.typesafe.config.ConfigFactory
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.config.HoconApplicationConfig
import io.ktor.features.CallLogging
import io.ktor.features.DefaultHeaders
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.server.engine.applicationEngineEnvironment
import io.ktor.server.engine.connector
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.slf4j.event.Level
import ru.rolsoft.moneyapp.service.accounts.Api.PREFIX
import ru.rolsoft.moneyapp.service.accounts.account.accountApi
import ru.rolsoft.moneyapp.service.accounts.transaction.transactionApi
import ru.rolsoft.moneyapp.service.accounts.user.userApi

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */

fun main() {
    val configuredPort = System.getenv("PORT")?.toInt() ?: 8088

    embeddedServer(Netty, applicationEngineEnvironment {
        config = HoconApplicationConfig(ConfigFactory.load())
        module {
            moneyAppModule()
        }
        connector {
            port = configuredPort
            host = "0.0.0.0"
        }
    }).start(wait = true)
}

object Api {
    val PREFIX = "/api/v1"
}

fun Application.moneyAppModule() {
    install(DefaultHeaders)
    install(CallLogging) {
        level = Level.INFO
    }
    installJackson()
    installStatusPages()
    installKoin()

    routing {
        route(PREFIX) {
            userApi()
            accountApi()
            transactionApi()
        }
    }

}

open class MoneyAppException(msg: String?, ex: Throwable?) : RuntimeException(msg, ex) {
    constructor(ex: Throwable) : this(null, ex)
    constructor(msg: String) : this(msg, null)
}
