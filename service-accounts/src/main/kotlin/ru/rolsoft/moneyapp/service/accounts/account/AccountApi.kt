package ru.rolsoft.moneyapp.service.accounts.account

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import org.koin.ktor.ext.inject
import ru.rolsoft.moneyapp.service.accounts.badRequest

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
fun Route.accountApi() {
    val service by inject<AccountsService>()

    route("accounts") {
        get("{accountId}") {
            try {
                val accountId = call.parameters["accountId"]

                when {
                    accountId.isNullOrEmpty() -> badRequest("AccountId should not be null/empty")
                    else -> {
                        val account = service.getAccount(accountId.toLong())
                        call.respond(account)
                    }
                }
            } catch (nfe: NumberFormatException) {
                badRequest("Account id should be long value")
            }
        }

        post {
            val accountRequest = call.receive<AccountRequest>()
            val createdAccount = service.createAccount(accountRequest.userId)
            call.respond(HttpStatusCode.Created, createdAccount)
        }
    }
}

data class AccountRequest(val userId: Long)