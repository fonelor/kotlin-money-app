package ru.rolsoft.moneyapp.service.accounts

import io.kotlintest.fail
import io.kotlintest.matchers.doubles.shouldNotBeGreaterThan
import io.kotlintest.matchers.numerics.shouldBeGreaterThan
import ru.rolsoft.moneyapp.database.ConcurrentTransactionException
import ru.rolsoft.moneyapp.database.db.Tables.ACCOUNTS
import ru.rolsoft.moneyapp.database.db.tables.daos.DbListener
import ru.rolsoft.moneyapp.database.db.tables.daos.DbSpec
import ru.rolsoft.moneyapp.database.db.tables.pojos.AccountsEntity
import ru.rolsoft.moneyapp.service.accounts.transaction.NotEnoughMoneyException
import ru.rolsoft.moneyapp.service.accounts.transaction.TransactionService
import java.math.BigDecimal
import java.util.concurrent.CountDownLatch
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicLong
import java.util.stream.Collectors
import kotlin.concurrent.thread
import kotlin.math.absoluteValue

/**
 * Assume that if deadlock happens, than thread with  a deadlock won't process enough  transactions
 *
 * @author Sergey Filippov <rolenof@gmail.com>
 */

class TransactionServiceStressTest : DbSpec({
    should("be no dirty reads") {
        val concurrent = AtomicLong()
        val success = AtomicLong()

        val accounts = DbListener.createUserWith2Accounts(BigDecimal.TEN, BigDecimal.ZERO)

        val transactionService = TransactionService(DbListener.accountsDao, DbListener.transactionsDao)

        val run = AtomicBoolean(true)
        val allFinished = CountDownLatch(2)
        val failed = AtomicBoolean(false)

        val actorCounts = listOf(
                startActor(run, allFinished, transactionService, accounts, success, concurrent),
                startActor(run, allFinished, transactionService, accounts, success, concurrent)
        )

        startArbiter(accounts, concurrent, success, failed, run, 100_000, BigDecimal.TEN)

        allFinished.await()

        if (failed.get()) fail("Dirty read occurred")

        val stats = actorCounts.stream().collect(Collectors.summarizingLong(AtomicLong::get))

        actorCounts.forEach { (it.toDouble() - stats.average).absoluteValue.shouldNotBeGreaterThan(stats.average * 0.05) }

        println(stats)

        success.get().shouldBeGreaterThan(8_000)
    }
    should("no deadlocks") {
        val concurrent = AtomicLong()
        val success = AtomicLong()

        val accounts: List<Long> =
                DbListener.createUserWith2Accounts(BigDecimal.TEN, BigDecimal.ZERO)
                        .plus(DbListener.createUserWith2Accounts(BigDecimal.TEN, BigDecimal.ZERO))

        val transactionService = TransactionService(DbListener.accountsDao, DbListener.transactionsDao)

        val run = AtomicBoolean(true)
        val allFinished = CountDownLatch(3)
        val failed = AtomicBoolean(false)

        val actorCounts = listOf(
                startActor(run, allFinished, transactionService, accounts, success, concurrent),
                startActor(run, allFinished, transactionService, accounts, success, concurrent),
                startActor(run, allFinished, transactionService, accounts, success, concurrent)
        )

        startArbiter(accounts, concurrent, success, failed, run, 100_000, BigDecimal(20))

        allFinished.await()

        if (failed.get()) fail("Dirty read occurred")

        val stats = actorCounts.stream().collect(Collectors.summarizingLong(AtomicLong::get))

        actorCounts.forEach { (it.toDouble() - stats.average).absoluteValue.shouldNotBeGreaterThan(stats.average * 0.05) }

        println(stats)

        success.get().shouldBeGreaterThan(8_000)
    }
})

private fun startArbiter(accounts: List<Long>,
                         concurrent: AtomicLong,
                         success: AtomicLong,
                         failed: AtomicBoolean,
                         run: AtomicBoolean,
                         iterations: Int,
                         expectedSum: BigDecimal) {
    thread(start = true) {
        try {
            repeat(iterations) { i ->
                val balances = DbListener.configuration.dsl()
                        .selectFrom(ACCOUNTS)
                        .where(ACCOUNTS.ID.`in`(accounts.toList()))
                        .fetchInto(AccountsEntity::class.java)
                if ((i % 1000L) == 0L) {
                    println("Concurrent ${concurrent.get()}, " +
                            "success ${success.get()}")
                }

                val sum = balances.map(AccountsEntity::getBalance).reduce(BigDecimal::add)

                if (sum.setScale(3) != expectedSum.setScale(3)) {
                    failed.set(true)
                    fail("Dirty read $balances")
                }
            }
        } finally {
            run.set(false)
        }
    }
}

private fun startActor(run: AtomicBoolean,
                       allFinished: CountDownLatch,
                       transactionService: TransactionService,
                       accounts: List<Long>,
                       success: AtomicLong,
                       concurrent: AtomicLong): AtomicLong {
    val actorCount = AtomicLong()
    thread(start = true) {
        try {
            while (run.get()) {
                try {
                    val fromAccId = ThreadLocalRandom.current().nextInt(0, accounts.size)
                    var toAccId: Int
                    do {
                        toAccId = ThreadLocalRandom.current().nextInt(0, accounts.size)
                    } while (fromAccId == toAccId)

                    transactionService.transfer(accounts[fromAccId], accounts[toAccId], BigDecimal.ONE)
                    success.incrementAndGet()
                } catch (_: NotEnoughMoneyException) {
                } catch (version: ConcurrentTransactionException) {
                    concurrent.incrementAndGet()
                }
                actorCount.incrementAndGet()
            }
        } finally {
            allFinished.countDown()
        }
    }
    return actorCount
}