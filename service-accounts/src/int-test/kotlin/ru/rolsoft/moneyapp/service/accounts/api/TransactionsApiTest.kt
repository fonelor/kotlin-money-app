package ru.rolsoft.moneyapp.service.accounts.api

import io.kotlintest.matchers.collections.shouldHaveSize
import io.kotlintest.matchers.string.shouldContain
import io.kotlintest.shouldBe
import io.ktor.http.HttpStatusCode
import ru.rolsoft.moneyapp.database.TransactionType
import ru.rolsoft.moneyapp.database.db.tables.daos.DbListener
import ru.rolsoft.moneyapp.database.db.tables.daos.DbSpec
import ru.rolsoft.moneyapp.database.db.tables.pojos.AccountsEntity
import ru.rolsoft.moneyapp.database.db.tables.pojos.TransactionsEntity
import ru.rolsoft.moneyapp.service.accounts.transaction.TRANSACTION_ID_HEADER
import ru.rolsoft.moneyapp.service.accounts.transaction.TransactionsHistory
import java.math.BigDecimal

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
class TransactionsApiTest : DbSpec({
    should("return 404") {
        withTestApplication {
            get("/api/v1/transactions/0") {
                response.status().shouldBe(HttpStatusCode.NotFound)
            }
        }
    }
    should("return 400 (transaction id should be long and not null)") {
        withTestApplication {
            get("/api/v1/transactions/trtr") {
                response.status().shouldBe(HttpStatusCode.BadRequest)
            }
        }
    }
    should("create transfer transaction") {
        DbListener.createUserWith2Accounts(BigDecimal.ONE, BigDecimal.TEN)
        withTestApplication {
            postJson("/api/v1/transactions", """
                {
                  "fromAcc": 0,
                  "toAcc": 1,
                  "amount": 1.00
                }
            """.trimIndent()) {
                response.status().shouldBe(HttpStatusCode.Created)
                response.headers[TRANSACTION_ID_HEADER]!!.toLong().shouldBe(0L)
                with(response.readBody<TransactionsEntity>()) {
                    id.shouldBe(0L)
                    source.shouldBe(0L)
                    sink.shouldBe(1L)
                    amount.shouldBe(BigDecimal.ONE.setScale(2))
                    type.shouldBe(TransactionType.TRANSFER.name)
                }
            }
        }
    }
    should("create deposit transaction") {
        DbListener.createUserWith2Accounts(BigDecimal.ONE, BigDecimal.TEN)
        withTestApplication {
            postJson("/api/v1/transactions/deposit", """
                {
                  "accountId": 0,
                  "amount": 1.00
                }
            """.trimIndent()) {
                response.status().shouldBe(HttpStatusCode.Created)
                response.headers[TRANSACTION_ID_HEADER]!!.toLong().shouldBe(0L)
                with(response.readBody<AccountsEntity>()) {
                    id.shouldBe(0L)
                    balance.shouldBe(BigDecimal(2).setScale(2))
                }
            }
            get("/api/v1/transactions/0") {
                response.status().shouldBe(HttpStatusCode.OK)
                with(response.readBody<TransactionsEntity>()) {
                    id.shouldBe(0L)
                    source.shouldBe(0L)
                    sink.shouldBe(0L)
                    amount.shouldBe(BigDecimal(1).setScale(2))
                    type.shouldBe(TransactionType.DEPOSIT.name)
                }
            }
        }
    }
    should("create withdraw transaction") {
        DbListener.createUserWith2Accounts(BigDecimal.ONE, BigDecimal.TEN)
        withTestApplication {
            postJson("/api/v1/transactions/withdraw", """
                {
                  "accountId": 0,
                  "amount": 1.00
                }
            """.trimIndent()) {
                response.status().shouldBe(HttpStatusCode.Created)
                response.headers[TRANSACTION_ID_HEADER]!!.toLong().shouldBe(0L)
                with(response.readBody<AccountsEntity>()) {
                    id.shouldBe(0L)
                    balance.shouldBe(BigDecimal(0).setScale(2))
                }
            }
            get("/api/v1/transactions/0") {
                response.status().shouldBe(HttpStatusCode.OK)
                with(response.readBody<TransactionsEntity>()) {
                    id.shouldBe(0L)
                    source.shouldBe(0L)
                    sink.shouldBe(0L)
                    amount.shouldBe(BigDecimal(1).setScale(2))
                    type.shouldBe(TransactionType.WITHDRAW.name)
                }
            }
        }
    }
    should("create withdraw, deposit and transfer transactions") {
        DbListener.createUserWith2Accounts(BigDecimal.ONE, BigDecimal.TEN)
        withTestApplication {
            postJson("/api/v1/transactions", """
                {
                  "fromAcc": 0,
                  "toAcc": 1,
                  "amount": 1.00
                }
            """.trimIndent())
            postJson("/api/v1/transactions/deposit", """
                {
                  "accountId": 0,
                  "amount": 1.00
                }
            """.trimIndent())
            postJson("/api/v1/transactions/withdraw", """
                {
                  "accountId": 0,
                  "amount": 1.00
                }
            """.trimIndent())
            get("/api/v1/transactions?accountId=0") {
                response.status().shouldBe(HttpStatusCode.OK)
                with(response.readBody<TransactionsHistory>()) {
                    transfers.shouldHaveSize(1)
                    withdraws.shouldHaveSize(1)
                    deposits.shouldHaveSize(1)

                    with(transfers[0]) {
                        source.shouldBe(0L)
                        sink.shouldBe(1L)
                        amount.shouldBe(BigDecimal.ONE.setScale(2))
                        type.shouldBe(ru.rolsoft.moneyapp.database.TransactionType.TRANSFER.name)
                    }

                    with(withdraws[0]) {
                        source.shouldBe(0L)
                        sink.shouldBe(0L)
                        amount.shouldBe(BigDecimal(1).setScale(2))
                        type.shouldBe(ru.rolsoft.moneyapp.database.TransactionType.WITHDRAW.name)
                    }

                    with(deposits[0]) {
                        source.shouldBe(0L)
                        sink.shouldBe(0L)
                        amount.shouldBe(BigDecimal(1).setScale(2))
                        type.shouldBe(ru.rolsoft.moneyapp.database.TransactionType.DEPOSIT.name)
                    }
                }
            }
        }
    }
    should("return 400 (not a transfer)") {
        withTestApplication {
            DbListener.createUserWith2Accounts(BigDecimal.ONE, BigDecimal.ONE)
            postJson("/api/v1/transactions", """
                {
                  "fromAcc": 0,
                  "toAcc": 0,
                  "amount": 1.00
                }
            """.trimIndent()) {
                response.status().shouldBe(HttpStatusCode.BadRequest)
                response.content.shouldContain("Use withdraw or deposit")
            }
        }
    }
    should("return 400") {
        withTestApplication {
            get("/api/v1/transactions") {
                response.status().shouldBe(HttpStatusCode.BadRequest)
                response.content.shouldContain("Account id should be submitted")
            }
            get("/api/v1/transactions?accountId=trrr") {
                response.status().shouldBe(HttpStatusCode.BadRequest)
                response.content.shouldContain("Account id should be long value")
            }
        }
    }
})