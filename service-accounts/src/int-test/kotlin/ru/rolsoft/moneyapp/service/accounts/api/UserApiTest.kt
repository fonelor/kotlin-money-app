package ru.rolsoft.moneyapp.service.accounts.api

import io.kotlintest.matchers.collections.shouldHaveSize
import io.kotlintest.matchers.string.shouldContain
import io.kotlintest.shouldBe
import io.ktor.http.HttpStatusCode
import org.koin.core.context.GlobalContext
import ru.rolsoft.moneyapp.database.db.tables.daos.DbListener
import ru.rolsoft.moneyapp.database.db.tables.daos.DbSpec
import ru.rolsoft.moneyapp.database.db.tables.pojos.AccountsEntity
import ru.rolsoft.moneyapp.database.db.tables.pojos.UsersEntity
import ru.rolsoft.moneyapp.service.accounts.transaction.TransactionService
import java.math.BigDecimal

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
class UserApiTest : DbSpec({
    should("return 404") {
        withTestApplication {
            get("/api/v1/users/0") {
                response.status().shouldBe(HttpStatusCode.NotFound)
            }
            patchJson("/api/v1/users/0", """
                        {
                            "lastName": "mmm"
                        }
            """.trimIndent()) {
                response.status().shouldBe(HttpStatusCode.NotFound)
                response.content.shouldContain(" is not found")
            }
        }
    }
    should("return bad request 400") {
        withTestApplication {
            postJson("/api/v1/users", """
                        {
                            "firstName": "firstName"
                        }
            """.trimIndent()) {
                response.status().shouldBe(HttpStatusCode.BadRequest)
            }
        }
    }
    should("return bad request 400 (invalid json)") {
        withTestApplication {
            postJson("/api/v1/users", """
                        {
                            "firstName": "firstName",
                        }
            """.trimIndent()) {
                response.status().shouldBe(HttpStatusCode.BadRequest)
            }
        }
    }
    should("create user") {
        withTestApplication {
            postJson("/api/v1/users", """
                        {
                            "firstName": "firstName",
                            "lastName": "lastName"
                        }
            """.trimIndent()) {
                response.status().shouldBe(HttpStatusCode.Created)
                with(response.readBody<UsersEntity>()) {
                    firstName.shouldBe("firstName")
                    lastName.shouldBe("lastName")
                    id.shouldBe(0L)
                }
            }
            get("/api/v1/users/0") {
                response.status().shouldBe(HttpStatusCode.OK)
                with(response.readBody<UsersEntity>()) {
                    firstName.shouldBe("firstName")
                    lastName.shouldBe("lastName")
                    id.shouldBe(0L)
                }
            }
        }
    }
    should("update user") {
        withTestApplication {
            postJson("/api/v1/users", """
                        {
                            "firstName": "firstName",
                            "lastName": "lastName"
                        }
            """.trimIndent()) {
                response.status().shouldBe(HttpStatusCode.Created)
                with(response.readBody<UsersEntity>()) {
                    firstName.shouldBe("firstName")
                    lastName.shouldBe("lastName")
                    id.shouldBe(0L)
                }
            }
            patchJson("/api/v1/users/0", """
                        {
                            "firstName": "nnn",
                            "lastName": "mmm"
                        }
            """.trimIndent()) {
                response.status().shouldBe(HttpStatusCode.OK)
                with(response.readBody<UsersEntity>()) {
                    firstName.shouldBe("nnn")
                    lastName.shouldBe("mmm")
                    id.shouldBe(0L)
                }
            }
        }
    }
    should("partially update user") {
        withTestApplication {
            postJson("/api/v1/users", """
                        {
                            "firstName": "firstName",
                            "lastName": "lastName"
                        }
            """.trimIndent()) {
                response.status().shouldBe(HttpStatusCode.Created)
                with(response.readBody<UsersEntity>()) {
                    firstName.shouldBe("firstName")
                    lastName.shouldBe("lastName")
                    id.shouldBe(0L)
                }
            }
            patchJson("/api/v1/users/0", """
                        {
                            "lastName": "mmm"
                        }
            """.trimIndent()) {
                response.status().shouldBe(HttpStatusCode.OK)
                with(response.readBody<UsersEntity>()) {
                    lastName.shouldBe("mmm")
                    id.shouldBe(0L)
                }
            }
        }
    }
    should("not update user") {
        withTestApplication {
            postJson("/api/v1/users", """
                        {
                            "firstName": "firstName",
                            "lastName": "lastName"
                        }
            """.trimIndent()) {
                response.status().shouldBe(HttpStatusCode.Created)
                with(response.readBody<UsersEntity>()) {
                    firstName.shouldBe("firstName")
                    lastName.shouldBe("lastName")
                    id.shouldBe(0L)
                }
            }
            patchJson("/api/v1/users/0", """
                        {
                        }
            """.trimIndent()) {
                response.status().shouldBe(HttpStatusCode.OK)
                with(response.readBody<UsersEntity>()) {
                    firstName.shouldBe("firstName")
                    lastName.shouldBe("lastName")
                    id.shouldBe(0L)
                }
            }
        }
    }
    should("return 2 accounts") {
        DbListener.createUserWith2Accounts(BigDecimal.ONE, BigDecimal.TEN)
        withTestApplication {
            val transactionsDao = GlobalContext.get().koin.get<TransactionService>()
            transactionsDao.transfer(0L, 1L, BigDecimal.ONE)
            transactionsDao.deposit(0L, BigDecimal.TEN)
            transactionsDao.withdraw(0L, BigDecimal(5))
            get("/api/v1/users/0/accounts") {
                response.status().shouldBe(HttpStatusCode.OK)
                val accounts = response.readBody<List<AccountsEntity>>()
                accounts.shouldHaveSize(2)
            }
        }
    }
})
