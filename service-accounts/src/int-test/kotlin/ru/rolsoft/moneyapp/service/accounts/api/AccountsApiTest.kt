package ru.rolsoft.moneyapp.service.accounts.api

import io.kotlintest.shouldBe
import io.ktor.http.HttpStatusCode
import ru.rolsoft.moneyapp.database.db.tables.daos.DbSpec
import ru.rolsoft.moneyapp.database.db.tables.pojos.AccountsEntity
import java.math.BigDecimal

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
class AccountsApiTest : DbSpec({
    should("return 404") {
        withTestApplication {
            get("/api/v1/accounts/0") {
                response.status().shouldBe(HttpStatusCode.NotFound)
            }
        }
    }
    should("return 404 (no such user)") {
        withTestApplication {
            postJson("/api/v1/accounts", """
                        {
                            "userId": 0
                        }
            """.trimIndent()) {
                response.status().shouldBe(HttpStatusCode.NotFound)
            }
        }
    }
    should("return 400 (userid should be long)") {
        withTestApplication {
            postJson("/api/v1/accounts", """
                        {
                            "userId": "qqq0"
                        }
            """.trimIndent()) {
                response.status().shouldBe(HttpStatusCode.BadRequest)
            }
        }
    }
    should("create an account") {
        withTestApplication {
            postJson("/api/v1/users", """
                        {
                            "firstName": "firstName",
                            "lastName": "lastName"
                        }
            """.trimIndent())
            postJson("/api/v1/accounts", """
                {
                    "userId": 0
                }
            """.trimIndent()) {
                response.status().shouldBe(HttpStatusCode.Created)
                with(response.readBody<AccountsEntity>()) {
                    id.shouldBe(0L)
                    userId.shouldBe(0L)
                    balance.shouldBe(BigDecimal.ZERO.setScale(2))
                    version.shouldBe(1L)
                }
            }
        }
    }
    should("return account") {
        withTestApplication {
            postJson("/api/v1/users", """
                        {
                            "firstName": "firstName",
                            "lastName": "lastName"
                        }
            """.trimIndent())
            postJson("/api/v1/accounts", """
                {
                    "userId": 0
                }
            """.trimIndent())
            get("/api/v1/accounts/0") {
                response.status().shouldBe(HttpStatusCode.OK)
                with(response.readBody<AccountsEntity>()) {
                    id.shouldBe(0L)
                    userId.shouldBe(0L)
                    balance.shouldBe(BigDecimal.ZERO.setScale(2))
                    version.shouldBe(1L)
                }
            }
        }
    }

})