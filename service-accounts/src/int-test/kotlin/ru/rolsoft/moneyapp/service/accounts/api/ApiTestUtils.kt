package ru.rolsoft.moneyapp.service.accounts.api

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.ktor.client.request.request
import io.ktor.config.MapApplicationConfig
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.server.testing.*
import ru.rolsoft.moneyapp.database.db.tables.daos.DbListener
import ru.rolsoft.moneyapp.service.accounts.moneyAppModule

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
fun withTestApplication(test: TestApplicationEngine.() -> Unit) {
    withTestApplication({
        (environment.config as MapApplicationConfig).apply {
            put("db.username", "sa")
            put("db.password", "sa")
            put("db.jdbcUrl", DbListener.getJdbcUrl())
        }
        moneyAppModule()
    }) {
        test()
    }
}

fun TestApplicationEngine.get(url: String, verify: TestApplicationCall.() -> Unit = {}) {
    with(handleRequest(HttpMethod.Get, url)) {
        verify()
    }
}

fun TestApplicationEngine.postJson(url: String, body: String, verify: TestApplicationCall.() -> Unit = {}) {
    json(HttpMethod.Post, url, body, verify)
}

fun TestApplicationEngine.patchJson(url: String, body: String, verify: TestApplicationCall.() -> Unit = {}) {
    json(HttpMethod.Patch, url, body, verify)
}

fun TestApplicationEngine.json(method: HttpMethod, url: String, body: String, verify: TestApplicationCall.() -> Unit = {}) {
    with(handleRequest(method, url) {
        request {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody(body)
        }
    }) {
        verify()
    }
}

val objectMapper = ObjectMapper()
        .registerModule(Jdk8Module())
        .registerModule(JavaTimeModule())
        .registerModule(KotlinModule())

inline fun <reified T> TestApplicationResponse.readBody(): T =
        ru.rolsoft.moneyapp.service.accounts.api.objectMapper.readValue(content, T::class.java)
