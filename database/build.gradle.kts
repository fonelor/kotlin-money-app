import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    api(project(":database-generated"))

    testApi("org.hsqldb:hsqldb:${project.extra["hsqldbVersion"]}")

    testApi("io.kotlintest:kotlintest-core:3.3.2")
    testApi("io.kotlintest:kotlintest-assertions:3.3.2")
    testApi("io.kotlintest:kotlintest-runner-junit5:3.3.2")
}

val tests: Configuration by configurations.creating

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "11"
    }

    withType<Test> {
        useJUnitPlatform()
    }

    val testJar by creating(Jar::class) {
        dependsOn("testClasses")
        archiveBaseName.set("test-${project.name}")
        from(sourceSets.test.get().output)
    }

    artifacts.add(tests.name, testJar)
}
