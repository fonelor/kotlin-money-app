package ru.rolsoft.moneyapp.database.daos

import org.jooq.Configuration
import org.jooq.Field
import org.jooq.impl.DSL
import ru.rolsoft.moneyapp.database.NoSuchEntityException
import ru.rolsoft.moneyapp.database.db.Tables.USERS
import ru.rolsoft.moneyapp.database.db.tables.daos.UsersDao
import ru.rolsoft.moneyapp.database.db.tables.pojos.UsersEntity

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
class UsersDaoExt(configuration: Configuration) : UsersDao(configuration) {
    fun updateUser(userId: Long, firstName: String?, lastName: String?): UsersEntity {
        val values = mutableListOf<String>()
        val fields = mutableListOf<Field<String>>()

        addFieldSetIfNotEmpty(firstName, USERS.FIRST_NAME, values, fields)
        addFieldSetIfNotEmpty(lastName, USERS.LAST_NAME, values, fields)

        if (values.isNotEmpty()) {
            if (configuration().dsl().update(USERS).set(DSL.row(fields), DSL.row(values))
                            .where(USERS.ID.eq(userId))
                            .execute() != 1) throw NoSuchEntityException(userId, USERS)
        }

        return fetchOneById(userId) ?: throw NoSuchEntityException(userId, USERS)
    }

    private fun addFieldSetIfNotEmpty(value: String?, field: Field<String>, values: MutableList<String>, fields: MutableList<Field<String>>) {
        if (!value.isNullOrEmpty()) {
            values.add(value)
            fields.add(field)
        }
    }
}