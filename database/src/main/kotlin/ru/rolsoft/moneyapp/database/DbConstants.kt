package ru.rolsoft.moneyapp.database

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
object DBConstants {
    const val SCHEMA_NAME: String = "MONEY_APP"
    const val CREATE_SCHEMA_SQL: String = "CREATE SCHEMA IF NOT EXISTS $SCHEMA_NAME"
}

enum class TransactionType(val value: Int) {
    WITHDRAW(0),
    DEPOSIT(1),
    TRANSFER(2)
}