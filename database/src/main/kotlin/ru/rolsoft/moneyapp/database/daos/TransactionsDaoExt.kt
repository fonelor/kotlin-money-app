package ru.rolsoft.moneyapp.database.daos

import org.jooq.Configuration
import org.jooq.impl.DSL
import ru.rolsoft.moneyapp.database.db.Tables
import ru.rolsoft.moneyapp.database.db.tables.daos.TransactionsDao
import ru.rolsoft.moneyapp.database.db.tables.pojos.TransactionsEntity

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
class TransactionsDaoExt(configuration: Configuration) : TransactionsDao(configuration) {

    fun fetchByAccountId(accId: Long, limit: Int = 100, fromId: Long?): List<TransactionsEntity> {
        val select = DSL.using(configuration())
                .selectFrom(Tables.TRANSACTIONS)
                .where(Tables.TRANSACTIONS.SOURCE.eq(accId).or(Tables.TRANSACTIONS.SINK.eq(accId)))
                .orderBy(Tables.TRANSACTIONS.ID.asc())
        val query = if (fromId == null) {
            select.limit(limit)
        } else {
            select.seek(fromId).limit(limit)
        }

        return query.fetch().map(mapper())
    }
}
