package ru.rolsoft.moneyapp.database

import ru.rolsoft.moneyapp.database.db.tables.pojos.AccountsEntity
import ru.rolsoft.moneyapp.database.db.tables.pojos.TransactionsEntity
import ru.rolsoft.moneyapp.database.db.tables.pojos.UsersEntity
import java.math.BigDecimal
import java.time.LocalDateTime

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
fun depositTransaction(accId: Long, amount: BigDecimal): TransactionsEntity =
        TransactionsEntity(null, accId, accId, amount, LocalDateTime.now(), TransactionType.DEPOSIT.name)

fun withdrawTransaction(accId: Long, amount: BigDecimal): TransactionsEntity =
        TransactionsEntity(null, accId, accId, amount, LocalDateTime.now(), TransactionType.WITHDRAW.name)

fun transferTransaction(from: Long, to: Long, amount: BigDecimal): TransactionsEntity =
        TransactionsEntity(null, from, to, amount, LocalDateTime.now(), TransactionType.TRANSFER.name)

fun newUser(firstName: String, lastName: String) = UsersEntity(null, firstName, lastName)

fun newAccount(userId: Long, balance: BigDecimal = BigDecimal.ZERO) = AccountsEntity(null, userId, balance, 1L)