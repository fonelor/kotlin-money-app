package ru.rolsoft.moneyapp.database.daos

import org.jooq.Configuration
import org.jooq.DSLContext
import ru.rolsoft.moneyapp.database.NoSuchEntityException
import ru.rolsoft.moneyapp.database.VersionConflictException
import ru.rolsoft.moneyapp.database.db.Tables
import ru.rolsoft.moneyapp.database.db.tables.daos.AccountsDao
import ru.rolsoft.moneyapp.database.db.tables.pojos.AccountsEntity
import java.math.BigDecimal

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
class AccountsDaoExt(configuration: Configuration) : AccountsDao(configuration) {

    fun updateWithVersionIncrement(accId: Long, deltaAmount: BigDecimal): AccountsEntity {
        val account = fetchOneById(accId) ?: throw NoSuchEntityException(accId, Tables.ACCOUNTS)
        return updateWithVersionIncrement(accId, deltaAmount, account.version)
    }

    fun updateWithVersionIncrement(accId: Long, deltaAmount: BigDecimal, version: Long): AccountsEntity {
        return updateWithVersionIncrement(accId, deltaAmount, version, configuration().dsl())
    }

    fun updateWithVersionIncrement(accId: Long, deltaAmount: BigDecimal, version: Long, inner: DSLContext): AccountsEntity {
        if (inner.update(Tables.ACCOUNTS)
                        .set(Tables.ACCOUNTS.BALANCE, Tables.ACCOUNTS.BALANCE.plus(deltaAmount))
                        .set(Tables.ACCOUNTS.VERSION, version + 1)
                        .where(Tables.ACCOUNTS.ID.eq(accId))
                        .and(Tables.ACCOUNTS.VERSION.eq(version))
                        .execute() == 1) {
            return fetchOneById(accId) ?: throw NoSuchEntityException(accId, Tables.ACCOUNTS)
        } else {
            throw VersionConflictException(accId, version, Tables.ACCOUNTS)
        }
    }
}
