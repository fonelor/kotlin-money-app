package ru.rolsoft.moneyapp.database.daos

import org.jooq.*
import org.jooq.exception.DataAccessException
import org.jooq.impl.DAOImpl
import org.jooq.impl.DSL.using
import ru.rolsoft.moneyapp.database.ConcurrentTransactionException
import ru.rolsoft.moneyapp.database.VersionConflictException
import java.sql.SQLTransactionRollbackException

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */

const val RETRY_LIMIT = 100

fun <R : UpdatableRecord<R>, P, T, TR> DAOImpl<R, P, T>.inTransaction(callable: TransactionalCallable<TR>): TR {
    return using(configuration()).transactionResult(callable)
}

fun <R : UpdatableRecord<R>, P, T> DAOImpl<R, P, T>.inTransaction(runnable: TransactionalRunnable) {
    using(configuration()).transaction(runnable)
}

fun <R : UpdatableRecord<R>, P, T, TR> DAOImpl<R, P, T>.inTransactionWithRetry(callable: ContextTransactionalCallable<TR>): TR {
    repeat(RETRY_LIMIT) {
        try {
            return using(configuration()).transactionResult(callable)
        } catch (versionEx: VersionConflictException) {
        } catch (rollback: DataAccessException) {
            // ignore
            if (rollback.cause !is SQLTransactionRollbackException) {
                throw rollback
            }
        }
    }
    throw ConcurrentTransactionException(table, RETRY_LIMIT)
}


fun <R : UpdatableRecord<R>, P, T> DAOImpl<R, P, T>.inTransaction(runnable: ContextTransactionalRunnable) {
    using(configuration()).transaction(runnable)
}

/**
 * A little hack to get generated id from db with jooq
 *
 * @param entity entity to store, entity id will be returned
 */
@Suppress("UNCHECKED_CAST")
fun <R : UpdatableRecord<R>, P, T> DAOImpl<R, P, T>.insertReturningId(entity: P): T {
    return insertReturningId(entity, configuration().dsl())
}

@Suppress("UNCHECKED_CAST")
fun <R : UpdatableRecord<R>, P, T> DAOImpl<R, P, T>.insertReturningId(entity: P, inner: DSLContext): T {
    val newRecord = inner.newRecord(table, entity)
    return inner.insertInto(table).set(newRecord).returning(table.primaryKey.fields).fetchOne().get(0) as T
}