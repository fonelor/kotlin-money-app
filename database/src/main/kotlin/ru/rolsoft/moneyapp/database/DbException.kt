package ru.rolsoft.moneyapp.database

import org.jooq.Named

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */

class NoSuchEntityException(entityId: Long, table: Named) :
        DBException("Unable to get ${table.name} with id $entityId")

class VersionConflictException(entityId: Long, version: Long, table: Named) :
        DBException("Unable to update ${table.name} with id $entityId: version conflict. Tried to update with version $version")

class ConcurrentTransactionException(table: Named, times: Int) :
        DBException("Unable to execute transaction in ${table.name} for $times")

open class DBException(msg: String? = null, ex: Throwable? = null) : RuntimeException(msg, ex) {
    constructor(msg: String) : this(msg, null)
    constructor(ex: Throwable) : this(null, ex)
}
