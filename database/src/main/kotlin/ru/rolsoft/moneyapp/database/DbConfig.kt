package ru.rolsoft.moneyapp.database

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import liquibase.Contexts
import liquibase.Liquibase
import liquibase.database.DatabaseFactory
import liquibase.database.jvm.JdbcConnection
import liquibase.resource.ClassLoaderResourceAccessor
import org.jooq.Configuration
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.conf.RenderNameStyle
import org.jooq.conf.Settings
import org.jooq.impl.DSL
import org.jooq.impl.DataSourceConnectionProvider
import org.jooq.impl.DefaultConfiguration
import org.jooq.impl.ThreadLocalTransactionProvider
import org.slf4j.LoggerFactory
import java.sql.Connection
import java.time.Duration
import javax.sql.DataSource

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
fun dslContext(configuration: Configuration): DSLContext {
    return DSL.using(configuration)
}

fun configuration(dataSource: DataSource): Configuration {
    val settings = Settings()
    settings.renderNameStyle = RenderNameStyle.LOWER

    val transactionProvider = ThreadLocalTransactionProvider(DataSourceConnectionProvider(dataSource))

    val configuration = DefaultConfiguration()
    configuration.set(settings)
    configuration.set(SQLDialect.HSQLDB)
    configuration.set(transactionProvider)

    return configuration
}

fun dataSource(config: DbConfig): DataSource {
    val logger = LoggerFactory.getLogger("liquibase-migration")

    val hikariConfig = fromConfig(config)
    val dataSource = HikariDataSource(hikariConfig)

    val startTime = System.nanoTime()

    logger.info("Starting liquibase migration with $dataSource")
    dataSource.connection.use { connection ->
        connection.createStatement().execute(DBConstants.CREATE_SCHEMA_SQL)

        val liquibaseDatabase = DatabaseFactory.getInstance()
                .findCorrectDatabaseImplementation(JdbcConnection(connection))
        liquibaseDatabase.defaultSchemaName = DBConstants.SCHEMA_NAME

        val liquibase = Liquibase("liquibase/changelog.xml",
                ClassLoaderResourceAccessor(), liquibaseDatabase)
        liquibase.update(null as Contexts?)
    }

    val elapsed = Duration.ofNanos(System.nanoTime() - startTime)

    logger.info("Liquibase migration successfully finished in ${elapsed.toSeconds()}s ${elapsed.toMillisPart()}ms")

    return dataSource
}

private fun fromConfig(config: DbConfig): HikariConfig {
    val hikariConfig = HikariConfig()
    hikariConfig.jdbcUrl = config.jdbcUrl
    hikariConfig.username = config.userName
    hikariConfig.password = config.password
    hikariConfig.transactionIsolation = Connection.TRANSACTION_READ_COMMITTED.toString()
    hikariConfig.minimumIdle = 1
    hikariConfig.maximumPoolSize = config.connectionLimit
    return hikariConfig
}

data class DbConfig(val jdbcUrl: String,
                    val userName: String,
                    val password: String,
                    val connectionLimit: Int) {
    override fun toString(): String {
        return "DbConfig(jdbcUrl='$jdbcUrl', userName='$userName', password='***', connectionLimit=$connectionLimit)"
    }
}