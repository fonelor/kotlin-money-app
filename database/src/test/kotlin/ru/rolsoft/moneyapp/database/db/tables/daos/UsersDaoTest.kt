package ru.rolsoft.moneyapp.database.db.tables.daos

import io.kotlintest.matchers.types.shouldBeNull
import io.kotlintest.shouldBe
import ru.rolsoft.moneyapp.database.daos.insertReturningId
import ru.rolsoft.moneyapp.database.db.tables.daos.DbListener.usersDao
import ru.rolsoft.moneyapp.database.newUser

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
class UsersDaoTest : DbSpec({
    should("create a user") {
        val id = usersDao.insertReturningId(newUser("firstName", "LastName"))
        id.shouldBe(0L)

        usersDao.fetchOneById(id).apply {
            firstName.shouldBe("firstName")
            lastName.shouldBe("LastName")
        }
    }

    should("create 2 users") {
        val id = usersDao.insertReturningId(newUser("firstName", "LastName"))
        id.shouldBe(0L)

        usersDao.fetchOneById(id).apply {
            firstName.shouldBe("firstName")
            lastName.shouldBe("LastName")
        }

        val id2 = usersDao.insertReturningId(newUser("oneMoreName", "oneMoreLastName"))
        id2.shouldBe(1L)

        usersDao.fetchOneById(id2).apply {
            firstName.shouldBe("oneMoreName")
            lastName.shouldBe("oneMoreLastName")
        }
    }

    should("update user name") {
        val id = usersDao.insertReturningId(newUser("firstName", "LastName"))

        val usersEntity = usersDao.fetchOneById(id)
        usersEntity.apply {
            firstName = "AnotherName"
            lastName = "NotLastName"
        }
        usersDao.update(usersEntity)

        usersDao.fetchOneById(id).apply {
            firstName.shouldBe("AnotherName")
            lastName.shouldBe("NotLastName")
        }
    }

    should("not get user") {
        usersDao.fetchOneById(1L).shouldBeNull()
    }
})