package ru.rolsoft.moneyapp.database.db.tables.daos

import io.kotlintest.extensions.TestListener
import io.kotlintest.specs.AbstractShouldSpec
import io.kotlintest.specs.ShouldSpec

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
abstract class DbSpec(body: AbstractShouldSpec.() -> Unit = {}) : ShouldSpec(body) {
    override fun listeners(): List<TestListener> = listOf(DbListener)
}