package ru.rolsoft.moneyapp.database.db.tables.daos

import io.kotlintest.matchers.types.shouldBeNull
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import org.jooq.exception.DataAccessException
import org.jooq.exception.SQLStateClass
import ru.rolsoft.moneyapp.database.daos.insertReturningId
import ru.rolsoft.moneyapp.database.db.tables.daos.DbListener.accountsDao
import ru.rolsoft.moneyapp.database.db.tables.daos.DbListener.usersDao
import ru.rolsoft.moneyapp.database.newAccount
import ru.rolsoft.moneyapp.database.newUser
import java.math.BigDecimal

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
class AccountsDaoExtTest : DbSpec({
    should("create an account with noninteger balance") {
        val userId = usersDao.insertReturningId(newUser("firstName", "lastName"))
        val accId = accountsDao.insertReturningId(newAccount(userId, BigDecimal(0.25)))
        accId.shouldBe(0L)

        accountsDao.fetchOneById(accId).apply {
            this.userId.shouldBe(userId)
            balance.shouldBe(BigDecimal(0.25).setScale(3))
            version.shouldBe(1L)
        }
    }

    should("throw constraint violation") {
        val exception = shouldThrow<DataAccessException> {
            accountsDao.insertReturningId(newAccount(0L))
        }
        exception.sqlStateClass().shouldBe(SQLStateClass.C23_INTEGRITY_CONSTRAINT_VIOLATION)
    }

    should("not get an account") {
        accountsDao.fetchOneById(0L).shouldBeNull()
    }
})