package ru.rolsoft.moneyapp.database.db.tables.daos

import io.kotlintest.matchers.collections.shouldHaveSize
import io.kotlintest.matchers.date.shouldBeAfter
import io.kotlintest.matchers.date.shouldBeBefore
import io.kotlintest.shouldBe
import ru.rolsoft.moneyapp.database.TransactionType
import ru.rolsoft.moneyapp.database.daos.insertReturningId
import ru.rolsoft.moneyapp.database.db.tables.daos.DbListener.createUserWith2Accounts
import ru.rolsoft.moneyapp.database.db.tables.daos.DbListener.transactionsDao
import ru.rolsoft.moneyapp.database.db.tables.pojos.TransactionsEntity
import ru.rolsoft.moneyapp.database.depositTransaction
import ru.rolsoft.moneyapp.database.transferTransaction
import ru.rolsoft.moneyapp.database.withdrawTransaction
import java.math.BigDecimal
import java.time.LocalDateTime

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
class TransactionsDaoExtTest : DbSpec({
    should("create transaction with noninteger amount") {
        val accounts = createUserWith2Accounts(BigDecimal.ONE, BigDecimal.ZERO)

        val txId = transactionsDao.insertReturningId(transferTransaction(accounts[0], accounts[1], BigDecimal(0.5)))
        transactionsDao.fetchOneById(txId).apply {
            source.shouldBe(accounts[0])
            sink.shouldBe(accounts[1])
            amount.shouldBe(BigDecimal(0.5).setScale(3))
            type.shouldBe(TransactionType.TRANSFER.name)
            timestamp.shouldBeBefore(LocalDateTime.now())
            timestamp.shouldBeAfter(LocalDateTime.now().minusSeconds(3))
        }
    }

    should("fetch all transactions by account") {
        val accounts = createUserWith2Accounts(BigDecimal.TEN, BigDecimal.ZERO)

        repeat(10) {
            transactionsDao.insert(transferTransaction(accounts[0], accounts[1], BigDecimal.ONE))
            transactionsDao.insert(depositTransaction(accounts[1], BigDecimal.ONE))
            transactionsDao.insert(withdrawTransaction(accounts[1], BigDecimal(2)))
        }

        val transactions = transactionsDao.fetchByAccountId(accounts[1], 10, null)
        transactions.shouldHaveSize(10)

        val fullTransactionsList = transactions.plus(transactionsDao.fetchByAccountId(accounts[1], 100, transactions.last().id))
        fullTransactionsList.shouldHaveSize(30)
        val trMap = fullTransactionsList.groupBy(TransactionsEntity::getType)
        trMap.keys.shouldHaveSize(3)
        trMap[TransactionType.TRANSFER.name]!!.shouldHaveSize(10)
        trMap[TransactionType.DEPOSIT.name]!!.shouldHaveSize(10)
        trMap[TransactionType.WITHDRAW.name]!!.shouldHaveSize(10)

    }
})