package ru.rolsoft.moneyapp.database.db.tables.daos

import io.kotlintest.TestCase
import io.kotlintest.TestResult
import io.kotlintest.extensions.TestListener
import org.jooq.Configuration
import ru.rolsoft.moneyapp.database.DBConstants
import ru.rolsoft.moneyapp.database.DbConfig
import ru.rolsoft.moneyapp.database.configuration
import ru.rolsoft.moneyapp.database.daos.AccountsDaoExt
import ru.rolsoft.moneyapp.database.daos.TransactionsDaoExt
import ru.rolsoft.moneyapp.database.daos.insertReturningId
import ru.rolsoft.moneyapp.database.dataSource
import ru.rolsoft.moneyapp.database.db.tables.pojos.AccountsEntity
import ru.rolsoft.moneyapp.database.db.tables.pojos.UsersEntity
import java.math.BigDecimal

/**
 * @author Sergey Filippov <rolenof@gmail.com>
 */
object DbListener : TestListener {

    private val dbConfig = DbConfig(
            "jdbc:hsqldb:mem:test;user=sa;password=sa;shutdown=true",
            "sa", "sa",
            10
    )

    lateinit var configuration: Configuration
    lateinit var usersDao: UsersDao
    lateinit var accountsDao: AccountsDaoExt

    lateinit var transactionsDao: TransactionsDaoExt

    override fun beforeTest(testCase: TestCase) {
        before()
    }

    @JvmStatic
    fun before() {
        println("Run liquibase migration")
        configuration = configuration(dataSource(dbConfig))

        usersDao = getUserDao()
        accountsDao = getAccountsDao()
        transactionsDao = getTransactionsDao()
    }

    @JvmStatic
    fun createUserWith2Accounts(accOneBalance: BigDecimal, accTwoBalance: BigDecimal): List<Long> {
        val userId = usersDao.insertReturningId(UsersEntity(null, "firstName", "lastName"))
        val accOneId = accountsDao.insertReturningId(AccountsEntity(null, userId, accOneBalance, 1))
        val accTwoId = accountsDao.insertReturningId(AccountsEntity(null, userId, accTwoBalance, 1))
        return listOf(accOneId, accTwoId)
    }

    @JvmStatic
    fun getJdbcUrl(): String = dbConfig.jdbcUrl

    override fun afterTest(testCase: TestCase, result: TestResult) {
        println("Drop schema cascade")
        configuration.dsl().dropSchema(DBConstants.SCHEMA_NAME).cascade().execute()
    }
}

private fun getAccountsDao(): AccountsDaoExt = AccountsDaoExt(DbListener.configuration)

private fun getTransactionsDao(): TransactionsDaoExt = TransactionsDaoExt(DbListener.configuration)

private fun getUserDao(): UsersDao = UsersDao(DbListener.configuration)