import liquibase.Contexts
import liquibase.Liquibase
import liquibase.database.core.HsqlDatabase
import liquibase.database.jvm.JdbcConnection
import liquibase.resource.FileSystemResourceAccessor
import org.hsqldb.jdbc.JDBCDriver
import org.jooq.codegen.GenerationTool
import org.jooq.meta.jaxb.*
import org.jooq.meta.jaxb.Configuration
import org.jooq.meta.jaxb.Target

val jdbcUrl = "jdbc:hsqldb:file:${project.buildDir.path}/db;user=sa;password=sa;shutdown=true"
val schemaName = "MONEY_APP"
val jooqPath = "${project.buildDir.path}/jooq"

buildscript {
    dependencies {
        classpath("org.hsqldb:hsqldb:${project.extra["hsqldbVersion"]}")
        classpath("org.liquibase:liquibase-core:${project.extra["liquibaseVersion"]}")
        classpath("org.jooq:jooq:${project.extra["jooqVersion"]}")
        classpath("org.jooq:jooq-codegen:${project.extra["jooqVersion"]}")
        classpath("org.jooq:jooq-meta:${project.extra["jooqVersion"]}")
        classpath("org.jooq:jooq-meta-extensions:${project.extra["jooqVersion"]}")
    }
}

plugins {
    `java-library`
}

dependencies {
    api("org.jooq:jooq:${project.extra["jooqVersion"]}")
    api("org.liquibase:liquibase-core:${project.extra["liquibaseVersion"]}")
    api("com.zaxxer:HikariCP:${project.extra["hikariVersion"]}")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

project.sourceSets["main"].java.srcDir(jooqPath)

tasks {
    val liquibaseMigration by registering(Task::class) {
        inputs.file("${project.projectDir}/src/main/resources/liquibase/changelog.xml")
        outputs.dir("${project.buildDir.path}/test")

        doLast {
            val database = HsqlDatabase()
            JDBCDriver().connect(jdbcUrl, null
                    /*mapOf("user" to "SA",
                            "password" to "SA")
                            .toProperties()*/).use { connection ->
                connection.createStatement().execute("CREATE SCHEMA IF NOT EXISTS  $schemaName")

                database.connection = JdbcConnection(connection)
                database.defaultSchemaName = schemaName
                val liquibase = Liquibase("${project.projectDir}/src/main/resources/liquibase/changelog.xml",
                        FileSystemResourceAccessor(),
                        database)
                liquibase.update(Contexts())
            }
        }
    }

    val jooqGeneration by creating(Task::class) {
        dependsOn(liquibaseMigration)
        inputs.dir("${project.buildDir.path}/test")
        val configuration = Configuration()
                .withJdbc(Jdbc()
                        .withDriver("org.hsqldb.jdbc.JDBCDriver")
                        .withPassword("sa")
                        .withUser("sa")
                        .withUrl(jdbcUrl))
                .withGenerator(Generator()
                        .withStrategy(Strategy()
                                .withMatchers(Matchers()
                                        .withTables(MatchersTableType()
                                                .withPojoClass(MatcherRule()
                                                        .withTransform(MatcherTransformType.PASCAL)
                                                        .withExpression("$0_Entity")))))
                        .withDatabase(Database()
                                .withForceIntegerTypesOnZeroScaleDecimals(false)
                                .withName("org.jooq.meta.hsqldb.HSQLDBDatabase")
                                .withIncludes(".*")
                                .withInputSchema(schemaName)
                                .withExcludes("databasechangelog|databasechangeloglock"))
                        .withGenerate(Generate()
                                .withGeneratedAnnotation(false)
                                .withJavaTimeTypes(true)
                                .withRelations(true)
                                .withDeprecated(false)
                                .withRecords(true)
                                .withFluentSetters(true)
                                .withDaos(true))
                        .withTarget(Target()
                                .withPackageName("ru.rolsoft.moneyapp.database.db")
                                .withDirectory(jooqPath)))

        extra.set("path", jooqPath)

        doLast {
            GenerationTool.generate(configuration)
        }
    }

    withType<JavaCompile> {
        dependsOn(jooqGeneration)
    }
}
