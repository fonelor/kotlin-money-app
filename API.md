# API documentation

## Errors

In case of error an error object is returned with according http status code
```json
{
  "code": 404,
  "description": "Not Found",
  "message": "No account with id 100"
}
```

There are 3 apis: [UsersAPI](#usersapi), [AccountAPI](#accountsapi) and [TransactionsAPI](#transactionsapi)

## UsersAPI

base url: `/api/v1/users`

### Create user

```
POST /api/v1/users
Content-Type: application/json

{
  "firstName": "userName",
  "lastName": "userLastName"
}
```

creates user with `userName` as first name and `userLastName` as last name. 
If `firstName` or `lastName` if not specified an error with code 400 will be returned

Returns created user:
```json
{
  "id": 0,
  "firstName": "userName",
  "lastName": "userLastName"
}
```

### Get user by id

```http request
GET /api/v1/users/0
Accept: application/json
```

Returns user with id `0` or not found error with code 404
```json
{
  "id": 0,
  "firstName": "userName",
  "lastName": "userLastName"
}
```

### Update user

```http request
PATCH /api/v1/users/0
Content-Type: application/json

{
  "firstName": "mister",
  "lastName": "anoterLastName"
}
```

Updates user first name/last name. Both first name and last name are optional

Returns updated user with id `0` or not found error with code 404 if user is not found
```json
{
  "id": 0,
  "firstName": "mister",
  "lastName": "anoterLastName"
}
```

### Get user's accounts

```http request
GET /api/v1/users/0/accounts
Accept: application/json
```

Returns user's accounts (user id is `0`) or error not found with code 404 if there is no such user

```json
[
  {
    "id": 0,
    "userId": 0,
    "balance": "0.00",
    "version": 1
  },
  {
    "id": 1,
    "userId": 0,
    "balance": "0.00",
    "version": 1
  }
]
```

## AccountsAPI

base url: `/api/v1/accounts`

### Create account for user

```http request
POST /api/v1/accounts
Accept: application/json
Content-Type: application/json

{
  "userId": 0
}
```

Returns created account or not found error with code 404 if there is no such user

```json
{
    "id": 1,
    "userId": 0,
    "balance": "0.00",
    "version": 1
}
```

### Get account by id

```http request
GET /api/v1/accounts/1
Accept: application/json
```

Returns account with id `0` or not found error with code 404 if there is no such account

```json
{
  "id": 1,
  "userId": 0,
  "balance": "0.00",
  "version": 1
}
```

## TransactionsAPI

base url: `/api/v1/transactions`

### Execute transfer transaction

```http request
POST /api/v1/transaction
Content-Type: application/json

{
  "fromAcc": 0,
  "toAcc": 1,
  "amount": 10
}
```

Executes transfer transaction from account `0` to account `1`. 
Returns executed transaction and transaction id in `X-Transaction-Id` header
If any of submitted accounts doesn't exist a not found error with code 404 will be returned.
In case there is not enough money on account `0` a bad request error with code 400 will be returned

```json
{
  "id": 0,
  "source": 0,
  "sink": 1,
  "amount": "10.00",
  "timestamp": "2019-08-10T16:12:25.245956",
  "type": "TRANSFER"
}
```

### Execute deposit transaction

```http request
POST /api/v1/transactions/deposit
Content-Type: application/json
Accept: application/json

{
  "accountId": 0,
  "amount": 100
}
```

Executes deposit transaction. In case there is no account `0` not found error with code 404 will be returned
Returns updated account and transaction id in `X-Transaction-Id` header

```json
{
  "id": 0,
  "userId": 0,
  "balance": "100.00",
  "version": 3
}
```

### Execute withdraw transaction

```http request
POST /api/v1/transactions/withdraw
Content-Type: application/json
Accept: application/json

{
  "accountId": 0,
  "amount": 100
}
```

Executes withdraw transaction. 
In case there is no account `0` not found error with code 404 will be returned
In case there is not enough money on account `0` a bad request error with code 400 will be returned
Returns updated account and transaction id in `X-Transaction-Id` header

```json
{
  "id": 0,
  "userId": 0,
  "balance": "0.00",
  "version": 4
}
```

### Get transactions linked with account

```http request
GET /api/v1/transactions?accountId=0&pageSize=10&lastId=0
Accept: application/json
```

Returns transfer, deposit and withdraw transactions linked with account `0`.

`pageSize` - result size per request [optional]
`lastId` - id to start from, exclusive [optional]

In case there is no such account a not found error with code 404 will be returned

```json
{
  "transfers": [
    {
      "id": 2,
      "source": 0,
      "sink": 1,
      "amount": "10.00",
      "timestamp": "2019-08-10T16:24:12.034224",
      "type": "TRANSFER"
    }
  ],
  "deposits": [
    {
      "id": 0,
      "source": 0,
      "sink": 0,
      "amount": "100.00",
      "timestamp": "2019-08-10T16:23:58.509757",
      "type": "DEPOSIT"
    }
  ],
  "withdraws": [
    {
      "id": 1,
      "source": 0,
      "sink": 0,
      "amount": "10.00",
      "timestamp": "2019-08-10T16:24:08.564512",
      "type": "WITHDRAW"
    }
  ]
}
```

