# Sample app written in kotlin

Technologies used:

 - [Kotlin](http://kotlinlang.org/)
 - [Ktor](https://ktor.io/)
 - [Koin](https://insert-koin.io/)
 - Kotlin gradle dsl
 - [Liquibase](http://www.liquibase.org/)
 - [JOOQ](http://www.jooq.org/)
 - [HSQLDB](http://hsqldb.org/)
 
You can try this app here: https://service-accounts.herokuapp.com/

To create shadowJat/fatJar (standalone jar):

```bash
./gradlew shadowJar
java -jar service-accounts/build/libs/service-accounts-all.jar  # start created jar
```
    
To start server from gradle:

    ./gradlew run
    
Tests:

    ./gradlew test
    
Stress test (validate there is no dirty reads):

    ./gradlew stressTest
    
Integration test (test api, and returned json):

    ./gradlew intTest


`service-accounts/src/main/rest-api.http` is REST requests examples

[API documentation](./API.md)
